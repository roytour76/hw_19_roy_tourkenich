﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX1
{
    public partial class Form2 : Form
    {
        public string date;
        private List<string> dateslist = new List<string>();
        public Form2(string name)
        {
            string line;
            InitializeComponent();
            System.IO.StreamReader file = new System.IO.StreamReader("C:\\Users\\magshimim\\Desktop\\" + name+"BD.txt");
            while ((line = file.ReadLine()) != null)
            {
                this.dateslist.Add(line);
            }
            file.Close();

        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {                     
            date = monthCalendar1.SelectionStart.ToShortDateString();


            for (int i = 0; i < this.dateslist.Count; i++)
            {
                string cdate = this.dateslist[i];
                int Start = cdate.IndexOf(",");
                string d = cdate.Substring(Start+1);
                if (d.Contains(date))
                    label2.Text= this.dateslist[i];
            }

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
